package br.com.apass.controlevacinacao.api.controle;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.apass.controlevacinacao.api.dao.AplicacaoVacinaDAO;
import br.com.apass.controlevacinacao.api.dao.UsuarioDAO;
import br.com.apass.controlevacinacao.api.modelo.AplicacaoVacina;

@RestController
@Transactional
public class AplicacaoVacinaController {

	@Autowired
	private AplicacaoVacinaDAO aplicacaoVacinaDao;

	@Autowired
	private UsuarioDAO usuarioDao;
	
	@PostMapping("/api/v1/aplicacaovacina")
	@ResponseStatus(code = HttpStatus.CREATED)
	public AplicacaoVacina cadastra(@Valid AplicacaoVacina aplicacaoVacina,  @NotBlank @Email String emailUsuario) {
		aplicacaoVacina.setUsuario(this.usuarioDao.pesquisaPorEmail(emailUsuario));
		this.aplicacaoVacinaDao.cadastra(aplicacaoVacina);
		return aplicacaoVacina;
	}
	
	@ExceptionHandler
	@ResponseStatus(code= HttpStatus.BAD_REQUEST)
	private Object handlerException(Exception exception, HttpServletResponse response) {
		return  "Erros de preenchimento de dados!";
	}
}
