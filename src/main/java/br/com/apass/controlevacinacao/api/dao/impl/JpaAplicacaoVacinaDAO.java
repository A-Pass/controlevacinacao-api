package br.com.apass.controlevacinacao.api.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import br.com.apass.controlevacinacao.api.dao.AplicacaoVacinaDAO;
import br.com.apass.controlevacinacao.api.modelo.AplicacaoVacina;

@Repository
public class JpaAplicacaoVacinaDAO implements AplicacaoVacinaDAO {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public void cadastra(AplicacaoVacina aplicacaoVacina) {
		this.manager.persist(aplicacaoVacina);
	}

}
