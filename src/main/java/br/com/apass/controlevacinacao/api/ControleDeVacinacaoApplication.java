package br.com.apass.controlevacinacao.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControleDeVacinacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControleDeVacinacaoApplication.class, args);
	}

}
