package br.com.apass.controlevacinacao.api.controle;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.apass.controlevacinacao.api.dao.UsuarioDAO;
import br.com.apass.controlevacinacao.api.modelo.Usuario;

@RestController
@Transactional
public class UsuarioController {
	
	@Autowired
	private UsuarioDAO dao;
	
	@PostMapping("/api/v1/usuario")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Usuario cadastra(Usuario usuario) {
		this.dao.cadastra(usuario);
		return usuario;
	}
}
