package br.com.apass.controlevacinacao.api.dao;

import br.com.apass.controlevacinacao.api.modelo.AplicacaoVacina;

public interface AplicacaoVacinaDAO {
	
	public void cadastra(AplicacaoVacina aplicacaoVacina);
	
}
