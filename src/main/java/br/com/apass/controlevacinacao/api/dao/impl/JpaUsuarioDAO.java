package br.com.apass.controlevacinacao.api.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import br.com.apass.controlevacinacao.api.dao.UsuarioDAO;
import br.com.apass.controlevacinacao.api.modelo.Usuario;

@Repository
public class JpaUsuarioDAO implements UsuarioDAO {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public void cadastra(Usuario usuario) {
		this.manager.persist(usuario);
	}

	@Override
	public Usuario pesquisaPorEmail(String email) {
		List<Usuario> resultado = this.manager.createQuery("select u from Usuario u where email = :email", Usuario.class)
				.setParameter("email", email)
				.getResultList();
		if(resultado.isEmpty())
			return null;
		return resultado.get(0);
	}

}
