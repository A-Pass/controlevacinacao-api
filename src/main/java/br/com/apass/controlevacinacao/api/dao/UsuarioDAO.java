package br.com.apass.controlevacinacao.api.dao;

import br.com.apass.controlevacinacao.api.modelo.Usuario;

public interface UsuarioDAO {
	
	public void cadastra(Usuario usuario);
	
	public Usuario pesquisaPorEmail(String email);
}
