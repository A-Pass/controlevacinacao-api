CREATE DATABASE controlevacinacao;

USE controlevacinacao;

CREATE TABLE usuario (
	id bigint NOT NULL AUTO_INCREMENT,
	nome varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	cpf char(11) NOT NULL,
	data_nascimento date NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT usuario_email_uq UNIQUE (email),
	CONSTRAINT usuario_cpf_uq UNIQUE (cpf)
);

CREATE TABLE aplicacao_vacina (
	id bigint NOT NULL AUTO_INCREMENT,
	nome_vacina varchar(255) NOT NULL,
	data_realizada date NOT NULL,
	usuario_id bigint NOT NULL,
	PRIMARY key(id),
	CONSTRAINT aplicacao_vacina_usuario_fk FOREIGN KEY (usuario_id) REFERENCES usuario (id)
);
